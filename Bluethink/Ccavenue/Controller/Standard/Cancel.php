<?php
 /**
 * @category   Bluethink
 * @package    Bluethink_Ccavenue
 * @author     Bluethink Team
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Bluethink\Ccavenue\Controller\Standard;

class Cancel extends \Bluethink\Ccavenue\Controller\CcavenueAbstract {

    /**
     * standerd checkout cancel operation
     *
     * @return redirected URL
     */
    public function execute() {
        $this->getOrder()->cancel()->save();
        $this->messageManager->addErrorMessage(__('Your order has been can cancelled'));
        $this->getResponse()->setRedirect(
                $this->getCheckoutHelper()->getUrl('checkout')
        );
    }
}
