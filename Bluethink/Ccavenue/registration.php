<?php
 /**
 * @category   Bluethink
 * @package    Bluethink_Ccavenue
 * @author     Bluethink Team
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Bluethink_Ccavenue',
    __DIR__
);
