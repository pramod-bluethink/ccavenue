/**
 * @category   Bluethink
 * @package    Bluethink_Ccavenue
 * @author     Bluethink Team
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
define([
    'uiComponent',
    'Magento_Checkout/js/model/payment/renderer-list'
],function(Component,renderList){
    'use strict';
    renderList.push({
        type : 'ccavenue',
        component : 'Bluethink_Ccavenue/js/view/payment/method-renderer/ccavenue-method'
    });

    return Component.extend({});
})
