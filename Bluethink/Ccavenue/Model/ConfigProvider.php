<?php
 /**
 * @category   Bluethink
 * @package    Bluethink_Ccavenue
 * @author     Bluethink Team
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Bluethink\Ccavenue\Model;

class ConfigProvider implements \Magento\Checkout\Model\ConfigProviderInterface
{
    protected $methodCode = \Bluethink\Ccavenue\Model\Ccavenue::PAYMENT_CCAVENUE_CODE;
    
    protected $method;
	
    public function __construct(\Magento\Payment\Helper\Data $paymenthelper){
        $this->method = $paymenthelper->getMethodInstance($this->methodCode);
    }

    /**
     * Get config values
     * 
     * @return array
     */
    public function getConfig(){

        return $this->method->isAvailable() ? [
            'payment'=>['ccavenue'=>[
                'redirectUrl'=>$this->method->getRedirectUrl()  
            ]
        ]
        ]:[];
    }
}
