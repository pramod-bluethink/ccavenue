<?php
/**
 * @category   Bluethink
 * @package    Bluethink_Ccavenue
 * @author     Bluethink Team
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Bluethink\Ccavenue\Model;

class Account implements \Magento\Framework\Option\ArrayInterface {

    const ACC_MONEY = 'ccavenue';
    /**
     * Possible environment types
     * 
     * @return array
     */
    public function toOptionArray() {
        return [
            [
                'value' => self::ACC_MONEY,
                'label' => 'Ccavenue'
            ]
        ];
    }
}
