<?php
 /**
 * @category   Bluethink
 * @package    Bluethink_Ccavenue
 * @author     Bluethink Team
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Bluethink\Ccavenue\Model;

use Magento\Sales\Api\Data\TransactionInterface;

class Ccavenue extends \Magento\Payment\Model\Method\AbstractMethod {

    const PAYMENT_CCAVENUE_CODE = 'ccavenue';
    const ACC_MONEY             = 'ccavenue';

    protected $_code            = self::PAYMENT_CCAVENUE_CODE;

    /**
     *
     * @var \Magento\Framework\UrlInterface 
     */
    protected $_urlBuilder;
    protected $_supportedCurrencyCodes = array(
        'AFN', 'ALL', 'DZD', 'ARS', 'AUD', 'AZN', 'BSD', 'BDT', 'BBD',
        'BZD', 'BMD', 'BOB', 'BWP', 'BRL', 'GBP', 'BND', 'BGN', 'CAD',
        'CLP', 'CNY', 'COP', 'CRC', 'HRK', 'CZK', 'DKK', 'DOP', 'XCD',
        'EGP', 'EUR', 'FJD', 'GTQ', 'HKD', 'HNL', 'HUF', 'INR', 'IDR',
        'ILS', 'JMD', 'JPY', 'KZT', 'KES', 'LAK', 'MMK', 'LBP', 'LRD',
        'MOP', 'MYR', 'MVR', 'MRO', 'MUR', 'MXN', 'MAD', 'NPR', 'TWD',
        'NZD', 'NIO', 'NOK', 'PKR', 'PGK', 'PEN', 'PHP', 'PLN', 'QAR',
        'RON', 'RUB', 'WST', 'SAR', 'SCR', 'SGF', 'SBD', 'ZAR', 'KRW',
        'LKR', 'SEK', 'CHF', 'SYP', 'THB', 'TOP', 'TTD', 'TRY', 'UAH',
        'AED', 'USD', 'VUV', 'VND', 'XOF', 'YER'
    );
    
    private $checkoutSession;

    /**
     * 
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory
     * @param \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory
     * @param \Magento\Payment\Helper\Data $paymentData
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Payment\Model\Method\Logger $logger
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param array $data
     */
      public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Payment\Model\Method\Logger $logger,
        \Bluethink\Ccavenue\Helper\Ccavenue $helper,
        \Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender,
        \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Directory\Model\CountryFactory $countryFactory
    ) {
        $this->helper = $helper;
        $this->orderSender = $orderSender;
        $this->httpClientFactory = $httpClientFactory;
        $this->checkoutSession = $checkoutSession;
        $this->_countryFactory = $countryFactory;

        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger
        );

    }

    /**
     * Check currency code usage
     *
     * @param string $currencyCode
     * @return boolean
     */
    public function canUseForCurrency($currencyCode) {
        if (!in_array($currencyCode, $this->_supportedCurrencyCodes)) {
            return false;
        }
        return true;
    }

    /**
     * Get redirect URL
     *
     * @return URL
     */
    public function getRedirectUrl() {
        return $this->helper->getUrl($this->getConfigData('redirect_url'));
    }

    /**
     * Get return URL
     *
     * @return URL
     */
    public function getReturnUrl() {
        return $this->helper->getUrl($this->getConfigData('return_url'));
    }

    /**
     * Get cancel URL
     *
     * @return URL
     */
    public function getCancelUrl() {
        return $this->helper->getUrl($this->getConfigData('cancel_url'));
    }

    /**
     * Return url according to environment
     * 
     * @return string
     */
    public function getCgiUrl() {
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();  
        $env = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('payment/ccavenue/environment');

        if ($env === 'production') {
            return $this->getConfigData('production_url');
        }elseif ($env === 'sandbox') {
            return $this->getConfigData('sandbox_url');
        }else{
            return $this->getConfigData('sandbox_url');
        }
    }

    /**
     * validate response
     * 
     * @return Boolean
     */
    public function validateResponse($returnParams) {
        $orderId = $this->checkoutSession->getLastRealOrder(); 
        if ($returnParams['orderNo'] == $orderId) {
            return true;
        }
        return true;
    }

    /**
     * Do post processing
     * 
     * @param $order, $payment, $response
     */
    public function postProcessing(\Magento\Sales\Model\Order $order,\Magento\Framework\DataObject $payment, $response) {
        
        $payment->setTransactionId($response['orderNo']);
        $payment->setTransactionAdditionalInfo('ccavenue_id',$response['orderNo']);
        $payment->setAdditionalInformation('ccavenue_order_status', 'approved');
        $payment->addTransaction(TransactionInterface::TYPE_ORDER);
        $payment->setIsTransactionClosed(0);
        $payment->place();
        $order->setStatus('processing');
        $order->save();
    }

    /**
     * prepare checkout requests
     * 
     * @return array
     */

    public function buildCheckoutRequest(){
        $order = $this->checkoutSession->getLastRealOrder();      
        $billing_address = $order->getBillingAddress();    

        $ccavenues = array();
        $ccavenues['merchant_id'] = $this->getConfigData("merchant_key");
        if ($this->getConfigData('account_type') == self::ACC_MONEY) {
            $ccavenues["service_provider"] = $this->getConfigData("service_provider");
        }
        $ccavenues["txnid"]           = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
        $ccavenues["amount"]          = round($order->getBaseGrandTotal(), 2);
        $ccavenues["order_id"]        = $this->checkoutSession->getLastRealOrderId();
        $ccavenues["billing_name"]    = $billing_address->getFirstName()." ".$billing_address->getLastname();
        $ccavenues['tid']             =  time() ;
        $ccavenues['currency']        =  $order->getOrderCurrencyCode() ;
        $ccavenues['language']        =  'EN' ;
        $ccavenues['billing_address'] = implode('', $billing_address->getStreet());
        $ccavenues["billing_city"]    = $billing_address->getCity();
        $ccavenues["billing_state"]   = $billing_address->getRegion();
        $ccavenues["billing_zip"]     = $billing_address->getPostcode();
        $ccavenues["billing_country"] = $this->_countryFactory->create()->loadByCode($billing_address->getCountryId())->getName();
        $ccavenues["billing_email"]   = $order->getCustomerEmail();
        $ccavenues['billing_tel']     = $billing_address->getTelephone();
        $ccavenues["phone"]           = $billing_address->getTelephone();
        
        $shippingAddress              = $order->getShippingAddress();
        if ( $shippingAddress )
        $shippingData = $shippingAddress->getData();
        $ccavenues['delivery_name']    = $shippingAddress->getFirstname() . ' ' . $shippingAddress->getLastname();
        $ccavenues['delivery_address'] = implode('', $shippingAddress->getStreet());
        $ccavenues['delivery_city']    = $shippingAddress->getCity();
        $ccavenues['delivery_state']   = $shippingAddress->getRegion();
        $ccavenues['delivery_zip']     =   $shippingAddress->getPostcode();
        $ccavenues['delivery_country'] = $shippingAddress->getCountryId();
        $ccavenues["delivery_country"] = $this->_countryFactory->create()->loadByCode($shippingAddress->getCountryId())->getName();
        $ccavenues['delivery_tel']     = $shippingAddress->getTelephone();
        $ccavenues["cancel_url"]       = $this->getCancelUrl();
        $ccavenues["furl_url"]         = $this->getReturnUrl();
        $ccavenues["redirect_url"]     = $this->getReturnUrl();
         
        $merchant_data = '' ;
        foreach ($ccavenues as $key => $value){
    
          $merchant_data.=$key.'='.urlencode($value).'&';
        }
        $workingKey               =  $this->getConfigData("working_key");
        $accessCode               =  $this->getConfigData("access_code");
        $encrypted_data           =  $this->encrypt($merchant_data,$workingKey);    
        $ccavenues['encRequest']  =  $encrypted_data ;
        $ccavenues['access_code'] =  $accessCode ;

        return $ccavenues;     
   } 

    /**
     * Encrypt plain tect with a key
     * 
     * @param $plainText, $key
     * @return string(Encrypted text)
     */
    private function encrypt($plainText,$key)
    {
        $secretKey = $this->hextobin(md5($key));
        $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
        $openMode = @mcrypt_module_open(MCRYPT_RIJNDAEL_128, '','cbc', '');
        $blockSize = @mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, 'cbc');
        $plainPad = $this->pkcs5_pad($plainText, $blockSize);
        if (@mcrypt_generic_init($openMode, $secretKey, $initVector) != -1) 
        {
            $encryptedText = @mcrypt_generic($openMode, $plainPad);
            @mcrypt_generic_deinit($openMode);
        } 
        return bin2hex($encryptedText);
    }

    /**
     * decrypt text with a key
     * 
     * @param $encryptedText, $key
     * @return string(plain text)
     */
    public function decrypt($encryptedText,$key)
    {
        $secretKey = $this->hextobin(md5($key));
        $initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
        $encryptedText=$this->hextobin($encryptedText);
        $openMode = @mcrypt_module_open(MCRYPT_RIJNDAEL_128, '','cbc', '');
        @mcrypt_generic_init($openMode, $secretKey, $initVector);
        $decryptedText = mdecrypt_generic($openMode, $encryptedText);
        $decryptedText = rtrim($decryptedText, "\0");
        @mcrypt_generic_deinit($openMode);
        return $decryptedText;
    }

    /**
     * Padding Function
     * 
     * @param $plainText, $blockSize
     * @return string
     */
    private function pkcs5_pad ($plainText, $blockSize)
    {
        $pad = $blockSize - (strlen($plainText) % $blockSize);
        return $plainText . str_repeat(chr($pad), $pad);
    }

    /**
     * Hexadecimal to Binary function for php 4.0 version
     * 
     * @param $hexString
     * @return string
     */
    private function hextobin($hexString){ 
        $length = strlen($hexString); 
        $binString="";   
        $count=0; 
        while($count<$length) 
        {       
            $subString =substr($hexString,$count,2);           
             $packedString = pack("H*",$subString); 
            if ($count==0)
            {
                $binString=$packedString;
            }else 
            {
                $binString.=$packedString;
            } 

        $count+=2; 
        } 
        return $binString; 
    } 
}
